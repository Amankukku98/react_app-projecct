import Notification from './Components/Notification';
import Navbar from './Components/Navbar/navbar';
import Header from './Components/Header/header';
import Layout from './Components/main_body/main';
// import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <><Notification />
      <Navbar />
      <div className='data-card'>
        <Header />
        <Layout />
      </div>
    </>

  );
}

export default App;
