import './header.css';

const Header = () => {
    return (
        <div className="header1">
            <h1 className='react'>React</h1>
            <p className='para'>A JavaScript library for building user interfaces</p>
            <div className="getStart">
                <button>Get Started</button>
                <a href="#">Take The Tutorial  </a>
            </div>
        </div>
    );
}
export default Header;