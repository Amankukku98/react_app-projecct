import './index.css';

const Notification=()=>{
    return(
        <div className="header">
        <span className="supportUkr">Support Ukraine 🇺🇦 </span>
        <span className="help">Help Provide Humanitarian Aid to Ukraine.</span>
    </div>
    );
}

export default Notification;